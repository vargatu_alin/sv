BEGIN;
use sv;
ALTER TABLE `locations_countries`
    MODIFY COLUMN `id` INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT;
CREATE INDEX `code` ON `locations_countries`(code);
COMMIT;