## Setup ##

```bash
cd sv
composer install
web/config.php -> set database credentials
db/ -> migrate sql files
php -S localhost:8081 -t web src/Client/index.html (Client)
php -S localhost:8082 -t web web/index.php (Server)

#Testing UI
    http://localhost:8081, you will see "search page"
#Testing Server
    CLI, run for example
    curl -G http://localhost:8082/index.php -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","id":1,"method":"getByCode","params":{"code":"RO"}}'
```