<?php
namespace SV\TEST\Server\WebApp\Service;

use Datto\JsonRpc\Exceptions\ArgumentException;
use SV\TEST\Server\WebApp\Entities\LocationCountry;
use SV\TEST\Server\WebApp\Repository\LocationCountryRepository;

class GetLocationCountryByCodeService
{
    private $locationCountryRepo;

    public function __construct(LocationCountryRepository $locationCountryRepo)
    {
        $this->locationCountryRepo = $locationCountryRepo;
    }

    public function execute($code)
    {
        /** @var LocationCountry $locationCountry */
        $locationCountry = $this->locationCountryRepo->findByCountryCode($code);
        if (! $locationCountry instanceof LocationCountry) {
            throw new ArgumentException();
        }
        return $locationCountry->jsonSerialize();
    }
}