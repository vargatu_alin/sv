<?php
require_once ROOT_PATH.'/vendor/autoload.php';
// Create Dependency injection container
$builder = new \DI\ContainerBuilder();
$builder->useAnnotations(true);
$builder->addDefinitions(__DIR__ . '/dependency.php');

global $objContainer;
$objContainer = $builder->build();