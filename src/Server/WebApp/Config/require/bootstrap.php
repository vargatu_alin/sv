<?php
require_once ROOT_PATH.'/vendor/autoload.php';
use Coach\Presentation\Web\Config\EntityManagerWrapper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
//require_once 'bootstrap.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = EntityManagerWrapper::getInstance();

return ConsoleRunner::createHelperSet($entityManager);