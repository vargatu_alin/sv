<?php

use Doctrine\ORM\EntityManager;
use SV\TEST\Server\WebApp\Config\EntityManagerWrapper;
use SV\TEST\Server\WebApp\Entities\LocationCountry;
use SV\TEST\Server\WebApp\Repository\LocationCountryRepository;

return [
    EntityManager::class => Di\factory(function (){
        return EntityManagerWrapper::getInstance();
    }),

    LocationCountryRepository::class => DI\factory(function () {
        $em = EntityManagerWrapper::getInstance();
        return $em->getRepository(LocationCountry::class);
    }),
];