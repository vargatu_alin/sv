<?php
namespace SV\TEST\Server\WebApp\Config;

class InjectionContainerWrapper
{
    public static function get($strClassName)
    {
        global $objContainer;
        return $objContainer->get($strClassName);
    }
}