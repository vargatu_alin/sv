<?php
namespace SV\TEST\Server\WebApp\Config;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerWrapper
{
    static private $objInstance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        $paths = array("../Entities");
        $isDevMode = false;

        if (! self::$objInstance) {
            $configList = unserialize(DB_CONNECTION);
            $dbParams = array(
                'driver'   => $configList['driver'],
                'user'     => $configList['username'],
                'password' => $configList['password'],
                'dbname'   => $configList['database'],
            );

            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
            self::$objInstance = EntityManager::create($dbParams, $config);
        }
        return self::$objInstance;
    }
}