<?php
namespace SV\TEST\Server\WebApp\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use JsonSerializable;

/**
 * LocationCountry
 *
 * @Entity(repositoryClass="SV\TEST\Server\WebApp\Repository\LocationCountryImpl")
 * @Table(name="locations_countries", uniqueConstraints={@UniqueConstraint(name="Uk_id", columns={"id"})})
 *
 */
class LocationCountry implements JsonSerializable
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @Column(name="prefix", type="string", length=255, nullable=true)
     */
    private $prefix;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function jsonSerialize()
    {
        return [
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'code'      => $this->getCode(),
            'prefix'    => $this->getPrefix()
        ];
    }
}