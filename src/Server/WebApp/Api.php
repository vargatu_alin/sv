<?php
namespace SV\TEST\Server\WebApp;

use Datto\JsonRpc\Evaluator;
use Datto\JsonRpc\Exceptions\ArgumentException;
use Datto\JsonRpc\Exceptions\MethodException;
use SV\TEST\Server\WebApp\Resource\LocationCountryResource;

class Api implements Evaluator {

    /** @var LocationCountryResource */
    private $resource;

    public function __construct(LocationCountryResource $resource)
    {
        $this->resource = $resource;
    }
    public function evaluate($method, $arguments)
    {
        if ($method === 'getByCode') {
            try {
                return $this->resource->getByCode($arguments);
            } catch (ArgumentException $e) {
            }
        }
        throw new MethodException();
    }
}