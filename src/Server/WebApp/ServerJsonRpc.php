<?php
namespace SV\TEST\Server\WebApp;

use Datto\JsonRpc\Server;

class ServerJsonRpc
{
    public function execute($resource, $message)
    {
        $server = new Server(new Api($resource));
        return $server->reply($message);
    }
}