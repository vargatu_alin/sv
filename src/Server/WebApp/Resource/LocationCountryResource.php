<?php
namespace SV\TEST\Server\WebApp\Resource;

use Datto\JsonRpc\Exceptions\ArgumentException;
use SV\TEST\Server\WebApp\Service\GetLocationCountryByCodeService;

class LocationCountryResource
{
    /** @var GetLocationCountryByCodeService */
    private $service;

    public function __construct(GetLocationCountryByCodeService $service)
    {
        $this->service = $service;
    }

    public function getByCode($arguments)
    {
        if (count($arguments) != 1) {
            throw new ArgumentException();
        }
        @list($code) = $arguments;
        return $this->service->execute($code);
    }
}
