<?php
namespace SV\TEST\Server\WebApp\Repository;

use Doctrine\ORM\EntityRepository;
use SV\TEST\Server\WebApp\Entities\LocationCountry;

class LocationCountryImpl extends EntityRepository implements LocationCountryRepository
{
    public function persist(LocationCountry $locationCounty)
    {
        $this->getEntityManager()->persist($locationCounty);
        $this->getEntityManager()->flush();
        $locationCounty->getId();
    }

    public function findByCountryCode($countryCode)
    {
        return parent::findOneBy(array('code' => $countryCode));
    }
}