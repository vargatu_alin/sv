<?php
namespace SV\TEST\Server\WebApp\Repository;

use SV\TEST\Server\WebApp\Entities\LocationCountry;

interface LocationCountryRepository
{
    public function persist(LocationCountry $locationCounty);
    public function findByCountryCode($countryCode);
}