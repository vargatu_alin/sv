<?php
use Datto\JsonRpc\Client;
use SV\TEST\Server\WebApp\Config\InjectionContainerWrapper;
use SV\TEST\Server\WebApp\Enum\ResourceEnum;
use SV\TEST\Server\WebApp\Resource\LocationCountryResource;
use SV\TEST\Server\WebApp\ServerJsonRpc;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin,X-Requested-With, Content-Type, Accept, Authorization');

require_once 'config.php';

$resources = [
  ResourceEnum::LOCATION_COUNTRY =>  InjectionContainerWrapper::get(LocationCountryResource::class)
];

foreach ($_GET as $key => $item) {
    $request = json_decode($key, true);
    if (isset($request['method']) && $request['method'] == 'getByCode') {
        $client = new Client();
        $client->query(1, $request['method'], [$request['params']['code']]);
        $message = $client->encode();

        $serverJsonRpc = new ServerJsonRpc();
        print_r($serverJsonRpc->execute($resources[ResourceEnum::LOCATION_COUNTRY], $message));
    }
}
