<?php
declare(strict_types=1);

define('ROOT_PATH', realpath('.'));
require_once ROOT_PATH.'/src/Server/WebApp/Config/require/injection.php';
define(
    'DB_CONNECTION',
    serialize(array(
            'driver'    => 'pdo_mysql',
            'database'  => 'sv',
            'username'  => 'root',
            'password'  => '',
        )
));