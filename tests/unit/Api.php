<?php
namespace util;

use Datto\JsonRpc\Evaluator;
use Datto\JsonRpc\Exceptions\ArgumentException;

class Api implements Evaluator
{
    public function evaluate($method, $arguments)
    {
        $locations = $this->getLocationsCountries();
        switch ($method) {
            case 'getByCode':
                return self::getByCode($arguments, $locations);
            default:
                throw new Exceptions\MethodException();
        }
    }

    private static function getByCode($arguments, $locations)
    {
        if (count($arguments) != 1) {
            throw new ArgumentException();
        }

        $argument = reset($arguments);
        foreach ($locations->countries as $country) {
            if ($country->code == $argument) {
                return $country;
            }
        }
        throw new ArgumentException();
    }

    private function getLocationsCountries()
    {
        return json_decode(file_get_contents('location_country.json'));
    }
}